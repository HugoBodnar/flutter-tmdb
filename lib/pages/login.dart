import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:toto/pages/moviesList.dart';

class Login extends StatelessWidget {
  Widget build(BuildContext context) {
    return LoginPage();
  }
}

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final _formKey = GlobalKey<FormState>();
  TextEditingController textController = TextEditingController();

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        extendBody: true,
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/mulan.jpg"),
                      fit: BoxFit.cover
                  )
              ),
            ),
            Column(
              children: [
                Container(height: MediaQuery.of(context).size.height * 0.4),
                Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                            colors: [Colors.black, Colors.transparent],
                            stops: [0.72, 1]
                        )
                    ),
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 100, 0, 0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                              "Login",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 30
                              ),
                              ),
                            )
                          ),
                          Form(
                              key: _formKey,
                              child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(0, 30, 0, 10),
                                      child: TextFormField(
                                        decoration: InputDecoration(
                                          fillColor: const Color(0xFF5E5E5E),
                                          filled: true,
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.white, width: 1.0),
                                            borderRadius: const BorderRadius.all(
                                              const Radius.circular(0.0),
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Color(0xFF8B8B8B), width: 1.0),
                                            borderRadius: const BorderRadius.all(
                                              const Radius.circular(0.0),
                                            ),
                                          ),
                                          border: new OutlineInputBorder(
                                            borderRadius: const BorderRadius.all(
                                              const Radius.circular(0.0),
                                            ),
                                          ),
                                          hintText: 'Email address'
                                        ),
                                        style: TextStyle(fontSize: 16.0, color: Colors.white),
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return 'Please enter some text';
                                          } else if(!EmailValidator.validate(value)) {
                                            return 'Please enter a valid email';
                                          }
                                          return null;
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                      child: TextFormField(
                                        obscureText: true,
                                        decoration: InputDecoration(
                                          fillColor: const Color(0xFF5E5E5E),
                                          filled: true,
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.white, width: 1.0),
                                            borderRadius: const BorderRadius.all(
                                              const Radius.circular(0.0),
                                            ),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Color(0xFF8B8B8B), width: 1.0),
                                            borderRadius: const BorderRadius.all(
                                              const Radius.circular(0.0),
                                            ),
                                          ),
                                          border: new OutlineInputBorder(
                                            borderRadius: const BorderRadius.all(
                                              const Radius.circular(0.0),
                                            ),
                                          ),
                                          hintText: 'Password'
                                        ),
                                        style: TextStyle(fontSize: 16.0, color: Colors.white),
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return 'Please enter some text';
                                          }
                                          return null;
                                        },
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                                      child: SizedBox(
                                        width: MediaQuery.of(context).size.width,
                                        height: 60,
                                        child: Builder(
                                          builder: (context) => RaisedButton(
                                            color: Color(0xFFFBC494),
                                            onPressed: () {
                                              if (_formKey.currentState.validate()) {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(builder: (context) => MoviesList())
                                                );
                                              }
                                            },
                                            child: Text('LOGIN'),
                                          ),
                                        )
                                      )
                                    ),
                                  ]
                              )
                          )
                        ],
                      ),
                    )
                )
              ],
            )
          ],
        ) // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
