import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:toto/classes/Movie.dart';
import 'package:toto/classes/TVShow.dart';
import 'package:toto/pages/details.dart';
import 'package:toto/pages/detailsTV.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;

class MoviesList extends StatelessWidget {
  Widget build(BuildContext context) {
    return MoviesListPage();
  }
}

Future fetchMovies() async {
  final response = await http.get('https://api.themoviedb.org/3/discover/movie?api_key=5cd271eab863832b66b095828a353fb7&sort_by=popularity.desc&include_adult=true&page=1&language=fr-FR');
  if (response.statusCode == 200) {
    var res = jsonDecode(response.body);
    var movies = [];
    res['results'].forEach((el) => movies.add(Movie.fromJson(el)));
    return movies;
  } else {
    throw Exception('Failed to load album');
  }
}

Future fetchTVShows() async {
  final response = await http.get('https://api.themoviedb.org/3/discover/tv?sort_by=vote_count.desc&api_key=5cd271eab863832b66b095828a353fb7&language=fr-FR');
  if (response.statusCode == 200) {
    var res = jsonDecode(response.body);
    var tvshows = [];
    res['results'].forEach((el) => tvshows.add(TVShow.fromJson(el)));
    return tvshows;
  } else {
    throw Exception('Failed to load album');
  }
}

Future fetchLastMovies() async {
  final response = await http.get('https://api.themoviedb.org/3/discover/movie?api_key=5cd271eab863832b66b095828a353fb7&sort_by=vote_average.desc&include_adult=true&include_video=true&vote_count.gte=5000&language=fr-FR');
  if (response.statusCode == 200) {
    var res = jsonDecode(response.body);
    var movies = [];
    res['results'].forEach((el) => movies.add(Movie.fromJson(el)));
    return movies;
  } else {
    throw Exception('Failed to load album');
  }
}

class MoviesListPage extends StatefulWidget {
  MoviesListPage({Key key}) : super(key: key);
  _MoviesListPageState createState() => _MoviesListPageState();
}

class _MoviesListPageState extends State<MoviesListPage> {
  Future _popularMovies = Future.delayed(
    Duration(seconds: 2),
        () => fetchMovies(),
  );

  Future _popularTVShows = Future.delayed(
    Duration(seconds: 2),
        () => fetchTVShows(),
  );

  Future _lastMovies = Future.delayed(
    Duration(seconds: 2),
        () => fetchLastMovies(),
  );

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Colors.black
        ),
        child: ListView(
          physics: ClampingScrollPhysics(),
          scrollDirection: Axis.vertical,
          children: [
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(16),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                        'Popular Movies',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold
                        )
                    ),
                  ),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: FutureBuilder(
                        future: _popularMovies,
                        builder: (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            return ListView.builder(
                              itemCount: snapshot.data.length,
                              shrinkWrap: true,
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                return Padding(
                                    padding: EdgeInsets.fromLTRB(16, 0, 8, 0),
                                    child: new GestureDetector(
                                      child: Container(
                                        width: MediaQuery.of(context).size.width * 0.4,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(5),
                                            image: DecorationImage(
                                                image: NetworkImage('${snapshot.data[index].image}'),
                                                fit: BoxFit.cover
                                            )
                                        ),
                                      ),
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => Details(movie: snapshot.data[index]))
                                        );
                                      },
                                    )
                                );
                              },
                            );
                          } else {
                            return Container();
                          }
                        },
                      )
                    )
                ),
                Padding(
                  padding: EdgeInsets.all(16),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                        'Popular TV Shows',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold
                        )
                    ),
                  ),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: FutureBuilder(
                          future: _popularTVShows,
                          builder: (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              return ListView.builder(
                                itemCount: snapshot.data.length,
                                shrinkWrap: true,
                                physics: BouncingScrollPhysics(),
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return Padding(
                                      padding: EdgeInsets.fromLTRB(16, 0, 8, 0),
                                      child: new GestureDetector(
                                        child: Container(
                                          width: MediaQuery.of(context).size.width * 0.4,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(5),
                                              image: DecorationImage(
                                                  image: NetworkImage('${snapshot.data[index].image}'),
                                                  fit: BoxFit.cover
                                              )
                                          ),
                                        ),
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context) => DetailsTV(tvshow: snapshot.data[index]))
                                          );
                                        },
                                      )
                                  );
                                },
                              );
                            } else {
                              return Container();
                            }
                          },
                        )
                    )
                ),
                Padding(
                  padding: EdgeInsets.all(16),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                        'Best Movies',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold
                        )
                    ),
                  ),
                ),
                Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: FutureBuilder(
                          future: _lastMovies,
                          builder: (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              return ListView.builder(
                                itemCount: snapshot.data.length,
                                shrinkWrap: true,
                                physics: BouncingScrollPhysics(),
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return Padding(
                                      padding: EdgeInsets.fromLTRB(16, 0, 8, 0),
                                      child: new GestureDetector(
                                        child: Container(
                                          width: MediaQuery.of(context).size.width * 0.4,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(5),
                                              image: DecorationImage(
                                                  image: NetworkImage('${snapshot.data[index].image}'),
                                                  fit: BoxFit.cover
                                              )
                                          ),
                                        ),
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context) => Details(movie: snapshot.data[index]))
                                          );
                                        },
                                      )
                                  );
                                },
                              );
                            } else {
                              return Container();
                            }
                          },
                        )
                    )
                ),
                SizedBox(width: 0, height: 30)
              ],
            )
          ],
        )
      )
    );
  }
}
