import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toto/classes/Movie.dart';
import 'dart:convert';
import 'dart:developer' as developer;

class Details extends StatelessWidget {
  final Movie movie;
  const Details({ Key key, this.movie }) : super(key: key);
  Widget build(BuildContext context) {
    return DetailsPage(movie: this.movie);
  }
}

Future fetchGenres(movie) async {
  final response = await http.get('https://api.themoviedb.org/3/genre/movie/list?api_key=5cd271eab863832b66b095828a353fb7&language=fr-FR');
  if (response.statusCode == 200) {
    var res = jsonDecode(response.body);
    var genres = List();
    res['genres'].forEach((el) => {
      if(movie.genres.indexOf(el['id']) != -1)
        genres.add(el)
    });
    return genres;
  } else {
    throw Exception('Failed to load album');
  }
}

Future fetchCasting(movie) async {
  final response = await http.get('https://api.themoviedb.org/3/movie/${movie.id}/credits?api_key=5cd271eab863832b66b095828a353fb7');
  if (response.statusCode == 200) {
    var res = jsonDecode(response.body);
    var cast = res['cast'][0]['name'];
    for(var i = 1; i < res['cast'].length; i++) {
      cast += ', ' + res['cast'][i]['name'];
    }
    return cast;
  } else {
    throw Exception('Failed to load album');
  }
}

class DetailsPage extends StatefulWidget {
  final Movie movie;
  DetailsPage({Key key, this.movie}) : super(key: key);
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  Widget build(BuildContext context) {
    Future _genres = Future.delayed(
      Duration(seconds: 2),
          () => fetchGenres(widget.movie),
    );
    Future _casting = Future.delayed(
      Duration(seconds: 2),
          () => fetchCasting(widget.movie),
    );

    return Scaffold(
        extendBodyBehindAppBar: true,
        extendBody: true,
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.transparent,
          elevation: 1.0,
        ),
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage('${widget.movie.image}'),
                      fit: BoxFit.cover
                  )
              ),
            ),
            Column(
              children: [
                Container(height: MediaQuery.of(context).size.height * 0.4),
                Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                            colors: [Colors.black, Colors.transparent],
                            stops: [0.72, 1]
                        )
                    ),
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                      child: Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.fromLTRB(0, 100, 0, 0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  widget.movie.title,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontSize: 30
                                  ),
                                ),
                              )
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                            child: Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Text(
                                    "15+",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Text(
                                    "-",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Text(
                                    widget.movie.date.substring(0, 4),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Text(
                                    "-",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14
                                    ),
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                    child: new Icon(Icons.star, color: Color.fromRGBO(210, 177, 137, 1), size: 16)
                                ),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: Text(
                                    widget.movie.vote.toString(),
                                    style: TextStyle(
                                        color: Color.fromRGBO(210, 177, 137, 1),
                                        fontSize: 14
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 34,
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: FutureBuilder(
                                  future: _genres,
                                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                                    if (snapshot.hasData) {
                                      return ListView.builder(
                                        itemCount: snapshot.data.length,
                                        shrinkWrap: true,
                                        physics: BouncingScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (context, index) {
                                          return Padding(
                                            padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                            child: DecoratedBox(
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius: BorderRadius.circular(4)
                                                ),
                                                child: Padding(
                                                    padding: EdgeInsets.all(10.0),
                                                    child: Text(snapshot.data[index]['name'], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12))
                                                )
                                            ),
                                          );
                                        },
                                      );
                                    } else {
                                      return Align(
                                          alignment: Alignment.topCenter,
                                          child: SizedBox(
                                              child: CircularProgressIndicator(
                                                  backgroundColor: Colors.white,
                                                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),
                                                  strokeWidth: 1.0,
                                              ),
                                              width: 20,
                                              height: 20
                                          )
                                      );
                                    }
                                  }
                              ),
                            )
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 15, 0, 10),
                            child: FutureBuilder(
                                future: _casting,
                                builder: (BuildContext context, AsyncSnapshot snapshot) {
                                  if (snapshot.hasData) {
                                    return RichText(
                                      maxLines: 2,
                                      text: TextSpan(
                                          children: [
                                            TextSpan(text: 'Casting : ', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                                            TextSpan(text: snapshot.data.toString(), style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic))
                                          ]
                                      ),
                                    );
                                  } else {
                                    return Align();
                                  }
                                }
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: RichText(text: TextSpan(text: "Résumé", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))),
                              )
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 20),
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: RichText(
                                    text: TextSpan(
                                      text: widget.movie.desc,
                                      style: TextStyle(
                                          color: Colors.white
                                      )
                                    ),
                                    maxLines: 5,
                                    overflow: TextOverflow.ellipsis,
                                  )
                              )
                          )
                        ],
                      ),
                    )
                )
              ],
            )
          ],
        ) // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
