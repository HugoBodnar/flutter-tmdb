class TVShow {
  final int id;
  final String name;
  final String image;
  final String date;
  final String desc;
  var vote;
  final List genres;

  TVShow({this.id, this.name, this.image, this.date, this.desc, this.vote, this.genres});

  factory TVShow.fromJson(Map<String, dynamic> json) {
    if(json['poster_path'] == null || json['poster_path'] == '') json['poster_path'] = "https://worldofvoz.files.wordpress.com/2020/01/http-error-404-not-found.png";
    else json['poster_path'] = "https://image.tmdb.org/t/p/w500/"+json['poster_path'];
    if(json['first_air_date'] == null || json['first_air_date'] == '') json['first_air_date'] = '----';
    return TVShow(
      id: json['id'],
      name: json['name'],
      image: json['poster_path'],
      date: json['first_air_date'],
      desc: json['overview'],
      vote: json['vote_average'],
      genres: json['genre_ids']
    );
  }
}