class Movie {
  final int id;
  final String title;
  final String image;
  final String date;
  final String desc;
  var vote;
  final List genres;

  Movie({this.id, this.title, this.image, this.date, this.desc, this.vote, this.genres});

  factory Movie.fromJson(Map<String, dynamic> json) {
    if(json['poster_path'] == null || json['poster_path'] == '') json['poster_path'] = "https://worldofvoz.files.wordpress.com/2020/01/http-error-404-not-found.png";
    else json['poster_path'] = "https://image.tmdb.org/t/p/w500/"+json['poster_path'];
    if(json['release_date'] == null || json['release_date'] == '') json['release_date'] = '----';
    return Movie(
      id: json['id'],
      title: json['title'],
      image: json['poster_path'],
      date: json['release_date'],
      desc: json['overview'],
      vote: json['vote_average'],
      genres: json['genre_ids']
    );
  }
}